var mp_range = $('#mp-range');
mp_range.html('');
noUiSlider.create(mp_range.height(136)[0], {
    start: [document.getElementsByTagName('audio')[0].volume * 100],
    connect: [true, false],
    direction: 'rtl',
    orientation: 'vertical',
    range: {
        'min': 0,
        'max': 100
    }
});
// "use strict";

(function(s, mp, $, io, store){

    Object.keys(mp.groups).forEach(function(groupCode){
        var group = mp.groups[groupCode];
        Object.keys(group.stations).forEach(function(stationCode){
            mp.stations[stationCode] = group.stations[stationCode];
        });
    });

    var alertIoEvents = $("#mp-app").data("alert-io-events");
    var prevStationCode;
    var prevSelectedStationCode;

    var escapeHTML = function(value){
        var entityMap = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#39;",
            "/": "&#x2F;"
        };
        return String(value).replace(/[&<>"'\/]/g, function (s) {
            return entityMap[s];
        });
    };

    mp.selectedStationCode = mp.currentStationCode;

    mp.hi_quality = false;

    mp.defaultLikeTtl = 3600;

    mp.getStationByCode = $.proxy(function(stationCode){
        return this.stations[stationCode];
    }, mp);

    mp.getCurrentStation = $.proxy(function(){
        return this.stations[this.currentStationCode];
    }, mp);

    mp.getCurrentStream = $.proxy(function(){
        var station = this.stations[this.currentStationCode];
        return (this.hi_quality && station.hq_stream) || station.stream;
    }, mp);

    mp.getCurrentGroup = $.proxy(function(){
        return this.groups[this.currentGroupCode];
    }, mp);

    mp.getSelectedStation = $.proxy(function(){
        return this.stations[this.selectedStationCode];
    }, mp);

    mp.getSelectedGroup = $.proxy(function(){
        return this.groups[this.stations[this.selectedStationCode].group];
    }, mp);

    mp.setCurrentStation = $.proxy(function(stationCode){
        if (stationCode === this.currentStationCode)
            return;
        if ( ! this.stations[stationCode])
            return;
        prevStationCode = this.currentStationCode;
        this.currentStationCode = stationCode;
        this.currentGroupCode = this.getCurrentStation().group;
        if (s.onlinePlayer.setVastSource)
        {
            s.onlinePlayer.setVastSource(this.getCurrentStation().vastUrl);
        }
        // Set title for lock screen audio player on the phones
        if (s.onlinePlayer.setTitle)
        {
            var label = [this.getCurrentGroup().title];
            if (this.getCurrentStation().type !== 'main')
            {
                label.push(' / ', this.getCurrentStation().title);
            }
            s.onlinePlayer.setTitle(label.join(''));
        }
    }, mp);

    mp.setSelectedStation = $.proxy(function(stationCode){
        if (stationCode === this.selectedStationCode)
            return;
        if ( ! this.stations[stationCode])
            return;
        prevSelectedStationCode = this.selectedStationCode;
        this.selectedStationCode = stationCode;
    }, mp);

    mp.autoPlay = $.proxy(function(stationCode){
        this.setCurrentStation(stationCode);
        this.syncView();
        this.setCurrentSliderStation();
        this.initStationSocket();
        if ($('.mp-overlay').hasClass('mp-show')) {
            mp.initSelectedSocket();
        }

        if (this.currentStationCode === stationCode)
        {
            var mediaConfig = this.getCurrentStream();
            s.onlinePlayer.start(mediaConfig);
        }
    }, mp);

    mp.init = $.proxy(function(){

        console.log('mp init');

        $('#mp-list').on('click.mp_list', function() {
            var el = $(this);
            if (el.hasClass('mp-active'))
            {
                el.removeClass('mp-active');
                $('.mp-list').removeClass('mp-active anim-stop');
                $('#mp-cover').removeClass('mp-active');
                el.next().text('Другие станции');
                $('.mp-overlay').removeClass('mp-show');
//				$('body').removeClass('mp-noscroll');
                if (mp.currentSocket)
                {
                    mp.currentSocket.disconnect();
                    mp.currentSocket = null;
                }
                mp.closeSelectedSocket();
            }
            else
            {
                el.addClass('mp-active');
                $('.mp-list').addClass('mp-active anim-stop');
                $('#mp-cover').addClass('mp-active');
                el.next().text('Свернуть');
                $('.mp-overlay').addClass('mp-show');
//				$('body').addClass('mp-noscroll');
                mp.initCurrentSocket();
                mp.initSelectedSocket();
            }
        });

        $('#mp-cover').on('click.mp_list', function() {
            $('#mp-list').triggerHandler('click.mp_list');
        });
        /*
        $('.mp-quality').on('click.hi_quality', function(){
            var el = $(this);
            var station = mp.getCurrentStation();
            var stream;
            el.toggleClass('mp-active');
            if (el.hasClass('mp-active'))
            {
                stream = station.hq_stream;
                mp.hi_quality = true;
            }
            else
            {
                stream = station.stream;
                mp.hi_quality = false;
            }
            s.onlinePlayer.start(stream);
        });
        */
        $('.mp-volume').on('click.mute', function(e) {
            if ($(e.target).closest('.mp-volume__edit').length)
                return;

            var el = $(this);
            el.toggleClass('mp-active');
            if (el.hasClass('mp-active'))
            {
                s.onlinePlayer.mute();
            }
            else
            {
                s.onlinePlayer.unmute();
            }
        });

        // volume range
        {
            var slider = $('#mp-range').height(136)[0];
            var startVolume = s.onlinePlayer.getVolume() || 99;
            if (typeof(startVolume) !== 'number')
            {
                startVolume = 99;
            }
            if (startVolume <= 0)
            {
                startVolume = 0;
            }

            noUiSlider.create(slider, {
                start: [+startVolume],
                connect: [true, false],
                direction: 'rtl',
                orientation: 'vertical',
                range: {
                    'min': 0,
                    'max': 100
                }
            });

            slider.noUiSlider.on('change', function(values){
//				console.log('change volume', values);
                s.onlinePlayer.setVolume(+values[0]);
                $('.mp-volume').removeClass('mp-active');
            });
            slider.noUiSlider.on('slide', function(values){
//				console.log('slide volume', values);
                s.onlinePlayer.setVolume(+values[0]);
            });
        }

        $('.mp-playlist').on('click.audio', '.mp-start', function(){
            var el = $(this).closest('.mp-playlist__item');

            if (el.hasClass('mp-disabled'))
                return;

            var audio = el.data('song-audio');
            if ( ! audio)
                return;

            if (el.hasClass('mp-active'))
            {
                el.removeClass('mp-active');
                s.onlinePlayer.stop();
            }
            else
            {
                el.siblings('.mp-active').removeClass('mp-active');
                var mediaConfig = {
                    source: (audio.indexOf("//") === 0) ? document.location.protocol+audio : audio,
                    mimeType: 'audio/mp3',
                    isFile: true
                };
                if (s.onlinePlayer.player.once)
                {
                    s.onlinePlayer.player.once(Clappr.Events.PLAYER_ENDED, audioFileEndedHandler);
                    s.onlinePlayer.player.once(Clappr.Events.PLAYER_STOP, audioFileEndedHandler);
                }
                else
                {
                    s.onlinePlayer.player.off(Clappr.Events.PLAYER_ENDED, audioFileEndedHandler);
                    s.onlinePlayer.player.on(Clappr.Events.PLAYER_ENDED, audioFileEndedHandler);
                    s.onlinePlayer.player.off(Clappr.Events.PLAYER_STOP, audioFileEndedHandler);
                    s.onlinePlayer.player.on(Clappr.Events.PLAYER_STOP, audioFileEndedHandler);
                }
                s.onlinePlayer.start(mediaConfig);
                el.addClass('mp-active');
            }
        });

        $('.mp-playlist').on('click.like', '.mp-like > .min, .mp-like > .max', mp.clickLikeThumbHandler);

        $('.js-btn-online', s.dom.online).on('click', function(e){
            e.preventDefault();
//			e.stopPropagation();

            if ($(this).hasClass('js-play'))
            {
                if (s.onlinePlayer.createError)
                {
                    s.onlinePlayer.showNoSolutionError();
                    return;
                }
                var mediaConfig = mp.getCurrentStream();
                s.onlinePlayer.start(mediaConfig);
            }
            else
            {
                s.onlinePlayer.stop();
            }
        });

        $('.js-sibling-stations-list', s.dom.online).on('click.station', '.js-sibling-station', function(e){
            e.preventDefault();
//			e.stopPropagation();
            var stationCode = $(this).data('station-code');
            var selectedStationIsChanged = false;
            if (mp.getSelectedStation().code !== stationCode)
            {
                mp.setSelectedStation(stationCode);
                selectedStationIsChanged = true;
            }
            if (mp.getCurrentStation().code === stationCode)
            {
                var isPlaying = (s.onlinePlayer.currentState === "play");
                if (isPlaying)
                {
                    s.onlinePlayer.stop();
                }
                else
                {
                    var mediaConfig = mp.getCurrentStream();
                    startOnlinePlayer(mediaConfig);
                }
                return;
            }
            mp.setCurrentStation(stationCode);
            mp.syncView();
            mp.initStationSocket();
            if (selectedStationIsChanged && $('.mp-overlay').hasClass('mp-show'))
            {
                mp.initSelectedSocket();
            }

            var mediaConfig = mp.getCurrentStream();
            startOnlinePlayer(mediaConfig);
        });

        $('.js-all-stations-list', s.dom.online).on('click.station', '.js-station', function(e){
            e.preventDefault();
            var stationCode = $(this).data('station-code');
            var selectedStationIsChanged = false;
            if (mp.getSelectedStation().code !== stationCode)
            {
                mp.setSelectedStation(stationCode);
                selectedStationIsChanged = true;
            }

            if ( $('body').width() > 736 && ! $(e.target).closest(".mp-radio__hover").length)
            {
                mp.syncView();
                if (selectedStationIsChanged && $('.mp-overlay').hasClass('mp-show'))
                {
                    mp.initSelectedSocket();
                }
                return;
            }

            var isPlaying = (s.onlinePlayer.currentState === "play");
            if (mp.getCurrentStation().code === stationCode)
            {
                if (isPlaying)
                {
                    s.onlinePlayer.stop();
                }
                else
                {
                    var mediaConfig = mp.getCurrentStream();
                    startOnlinePlayer(mediaConfig);
                }
            }
            else
            {
                mp.setCurrentStation(stationCode);
                mp.syncView();
                mp.setCurrentSliderStation();
                mp.initStationSocket();
                if (selectedStationIsChanged)
                {
                    mp.initSelectedSocket();
                }

                if (isPlaying)
                {
                    s.onlinePlayer.stop();
                }
                var mediaConfig = mp.getCurrentStream();
                startOnlinePlayer(mediaConfig);
            }
        });

        $('.mp-right', s.dom.online).on('click.hi_quality', '.mp-quality', function(e){
            e.preventDefault();
            var el = $(this);
            var station = mp.getCurrentStation();
            var stream;
            el.toggleClass('mp-active');
            if (el.hasClass('mp-active'))
            {
                stream = station.hq_stream;
                mp.hi_quality = true;
                $('.mp-quality').addClass('mp-active');
            }
            else
            {
                stream = station.stream;
                mp.hi_quality = false;
                $('.mp-quality').removeClass('mp-active');
            }
            startOnlinePlayer(stream);
        });

        $('.mp-right__button').on('click', function() {
            $(this).parents('.mp-right').toggleClass('mp-show');
        });

        $('.mp-right__close').on('click', function() {
            $(this).parents('.mp-right').removeClass('mp-show');
        });

        $('.mp-overlay').on('click', function(e) {
            if (e.target === this)
            {
                $('#mp-list').triggerHandler('click.mp_list');
            }
        });

        $('.js-station-playlist-link').on('click', function(){
            $('#mp-list').triggerHandler('click.mp_list');
        });

        this.renderSlider(true);
//		this.initSiblingStationsSlider();
//		this.initInfiniteScroll();

        this.syncView();
        this.initStationSocket();
        if ($('.mp-overlay').hasClass('mp-show')) {
            this.initSelectedSocket();
        }

        prevStationCode = this.currentStationCode;
        prevSelectedStationCode = this.setSelectedStationCode;

        if (s.onlinePlayer.setVastSource)
        {
            s.onlinePlayer.setVastSource(this.getCurrentStation().vastUrl);
        }

        var resizeTimeout = null;
        $(window).on("resize", function(){
            if (resizeTimeout) {
                window.clearTimeout(resizeTimeout);
            }
            resizeTimeout = setTimeout(function(){
                var $mpAlbum_photoImg = $('.mp-album_photo').find('img');
                if ( $('body').width() > 736 || $mpAlbum_photoImg.attr("src") != mp.stubImage) {
                    $mpAlbum_photoImg.show();
                } else {
                    $mpAlbum_photoImg.hide();
                }
            }, 100);
        });
    }, mp);

    var _startTimer;
    var _mediaConfig;
    function startOnlinePlayer(mediaConfig, delay){
        if (_startTimer)
        {
            window.clearTimeout(_startTimer);
        }
        _mediaConfig = mediaConfig;
        if (mediaConfig)
        {
            _startTimer = window.setTimeout(_startOnlinePlayer, delay || 400);
        }
    }

    function _startOnlinePlayer(){
        if (_mediaConfig)
        {
            s.onlinePlayer.start(_mediaConfig);
        }
    }

    mp.initSiblingStationsSlider = function(){
        var el = $('#mp-wrap');
        var countSlides = el.children('.mp-slide').length;

        var station = this.getCurrentStation();
        var group = this.groups[station.altGroup || station.group];
        var countStations = Object.keys(group.stations).length;

//		var minSlideWidth = 86;
//		var maxSlideWidth = 92;
        var minSlideWidth = 85;
        var maxSlideWidth = 85;
        el.children('.mp-slide')
            .css('width', '85px')
            .css('minWidth', '85px')
            .css('maxWidth', '85px');
        var maxVisibleSlides = Math.min(countStations, 9); //7;

        $('.mp-wrap').css('maxWidth', countStations >= 7 ? 'none' : '');

//		console.log('initSlickSlider', countSlides);

        var visibleCount = countStations;
        if (visibleCount > maxVisibleSlides)
        {
            visibleCount = maxVisibleSlides;
        }
        else if (visibleCount < 4)
        {
            visibleCount = 4;
        }

        $('.mp-carousel').css('maxWidth', (minSlideWidth * visibleCount) +'px');

        if (countSlides > 3)
        {
            var slidesToShow = Math.min(countSlides, maxVisibleSlides);
            var slickConfig = {
                centerMode: false,
                centerPadding: '40px',
                slidesToShow: slidesToShow,
                respondTo: 'slider',
                speed: 200,
                touchMove: true,
                touchThreshold: 500,
                arrows: false,
                swipe: true,
                swipeToSlide: true,
                variableWidth: true,
                infinite: true
            };
            var slickResponsive = [];

            slickResponsive.push({
                breakpoint: minSlideWidth * 3 - 1,
                settings: {
                    centerMode: false,
                    variableWidth: true,
                    slidesToShow: 1
                }
            });
            for (var i = 3; i <= maxVisibleSlides; i++)
            {
                slickResponsive.push({
                    breakpoint: (i + 0) * maxSlideWidth + 40,
                    settings: {
                        centerMode: false,
                        variableWidth: true,
                        slidesToShow: i
                    }
                });
            }

            slickConfig.responsive = slickResponsive.reverse();
//			console.log('slickConfig', slickConfig);

            function shiftFirstSlideToLeft()
            {
                var slick = el.slick('getSlick');
                var countBeforeCurrentSlide = slick.$slides.filter('.slick-current').prevAll('.slick-active').length;
                if (countBeforeCurrentSlide > 0)
                {
//					console.log('countBeforeCurrentSlide', countBeforeCurrentSlide);
                    el.slick('slickGoTo', countBeforeCurrentSlide, true);
                }
            }

            el.off('init.slick').on('init.slick', function(event, slick){
                slick.$slider.closest(".mp-wrap")
                    .find(".mp-wrap-prev,.mp-wrap-next").remove();
                window.setTimeout(shiftFirstSlideToLeft, 0);
            });

            el.off('breakpoint.slick').on('breakpoint.slick', function(event, slick, breakpoint){
//				console.log('breakpoint', event, slick, breakpoint);
            });

            el.slick(slickConfig);
        }
    };
    /*
        mp.initInfiniteScroll = function(){
            var mpRight = $('.mp-right');
            var mpRadio = mpRight.find('.mp-radio');
            var html = mpRadio.html();
            var height  = mpRadio.height();

            mpRadio.prepend(html);
            mpRadio.prepend(html);
            mpRadio.append(html);
            mpRadio.append(html);

            html = null;
            mpRadio = null;

            var _scrollTimer = 0;
            var _inProcessScroll = false;
            var _scrollCounter = 0;
            var debounceRatio = 10;

            function scrollHandler(){
    //			console.log('scrollHandler', _inProcessScroll);
                if (_inProcessScroll)
                    return;
                _inProcessScroll = true;
                var scrollTop = mpRight.scrollTop();
                if (scrollTop < height / 2)
                {
                    // top position
                    mpRight.scrollTop(height * 1.5);
                }
                else if (scrollTop + mpRight.height() >= height * 4)
                {
                    // bottom position
                    mpRight.scrollTop(scrollTop - height * 2);
                }
                _inProcessScroll = false;
                _scrollCounter = 0;
            }

            mpRight.scrollTop(height * 2);

            mpRight.on('scroll', function(){
    //			console.log('scroll event');
                if (_inProcessScroll)
                    return;
                if (_scrollTimer)
                {
                    clearTimeout(_scrollTimer);
                    _scrollTimer = 0;
                }
                ++_scrollCounter;
                if (_scrollCounter > debounceRatio)
                {
                    scrollHandler();
                }
                else
                {
                    _scrollTimer = window.setTimeout(scrollHandler, 50);
                }
            });
        };
    */
    function audioFileEndedHandler(){
        $('.mp-playlist .mp-active').removeClass('mp-active');
    }

    mp.syncView = $.proxy(function(){
        var currentGroup = this.getCurrentGroup();
        var currentStation = this.getCurrentStation();
        var selectedGroup = this.getSelectedGroup();
        var selectedStation = this.getSelectedStation();

        $('.js-all-stations-list').children('.js-station')
            .removeClass('mp-current')
            .filter('[data-station-code="'+selectedStation.code+'"]')
            .addClass('mp-current');

        $('.mp-quality')
        //			.toggle(!!station.hq_stream)
            .toggleClass('mp-active', this.hi_quality);
        if (selectedStation.type === 'web')
        {
            $('.js-station-group-title').html(escapeHTML(selectedGroup.title));
            $('.js-head-station-title').html(escapeHTML(selectedStation.title));
        }
        else
        {
            $('.js-station-group-title').html('');
            $('.js-head-station-title').html(escapeHTML(selectedGroup.title));
        }

        $('.mp-broadcast__artist').html(escapeHTML(selectedStation.title));
        $('.mp-broadcast__song').html(escapeHTML(selectedStation.slogan));

        if (currentStation.type === 'web')
        {
            $('.js-station-title').html('<b>' + escapeHTML(currentGroup.title) + '</b> <span>/ ' + escapeHTML(currentStation.title) + '</span>');

            $('.mp-reading__artist').html(escapeHTML(selectedStation.slogan));
            $('.mp-reading__song').html('');
        }
        else
        {
            $('.js-station-title').html('<b>' + escapeHTML(currentGroup.title) + '</b>');

            $('.mp-reading__artist').html(escapeHTML(selectedStation.slogan));
            $('.mp-reading__song').html(escapeHTML(selectedStation.title));
        }
        if (selectedGroup.link) {
            $('.js-station-group-link')
                .prop('href', selectedGroup.link)
                .show();
        } else {
            $('.js-station-group-link')
                .prop('href', "#")
                .hide();
        }
        $('.js-head-station-logo').prop({src: selectedStation.logo, alt: selectedGroup.title + ' / ' + selectedStation.title});
//		$('.js-station-logo').prop({src: station.logo, alt: group.title});
//		$('.js-station-logo').prop({src: this.stubImage, alt: ''});

        $('.js-station-playlist-title').html(selectedStation.showPrograms ? 'Программы' : 'Плейлист');

        {
            var link = selectedStation.playlistLink;
            var target = '_blank';
            var loc = document.location;
            if (link && link.charAt(0) === '/' && link.charAt(1) !== '/')
            {
                target = '_self';
            }
            else if (link && link.indexOf(loc.protocol+'//'+loc.host) === 0)
            {
                target = '_self';
            }
            $('.js-station-playlist-link')
                .prop('href', link || '#')
                .prop('target', target)
                .html(selectedStation.showPrograms ? 'Всё расписание' : 'Полный плейлист')
                .toggle(!!link);
        }

        this.renderSlider();
    }, mp);

    mp.setViewState =  $.proxy(function(state, mediaConfig){

        var station = this.getCurrentStation();

        if (mediaConfig && mediaConfig.isFile === true)
        {
            state = "pause";
        }
        else
        {
            $('.mp-playlist .mp-active').removeClass('mp-active');
        }

        if (state === "play")
        {
            // Sync online button
            $('.js-btn-online', s.dom.online)
                .removeClass('js-play')
                .addClass('mp-active js-stop');
            // Sync station list button
            $('.js-all-stations-list', s.dom.online)
                .children('.mp-active')
                .removeClass('mp-active')
                .end()
                .children('.js-station[data-station-code="'+station.code+'"]')
                .addClass('mp-active');
            // Sync sibling station list button
            $('.js-sibling-stations-list', s.dom.online)
                .find('.mp-active')
                .removeClass('mp-active')
                .end()
                .find('.js-sibling-station[data-station-code="'+station.code+'"]')
                .addClass('mp-active');
        }
        else
        {
            // Sync online button
            $('.js-btn-online', s.dom.online)
                .removeClass('mp-active js-stop')
                .addClass('js-play');
            // Sync station list button
            $('.js-all-stations-list', s.dom.online)
                .children('.mp-active')
                .removeClass('mp-active');
            $('.js-sibling-stations-list', s.dom.online)
                .find('.mp-active')
                .removeClass('mp-active');
        }
    }, mp);

    mp.isAndroid = /(android)/i.test(navigator.userAgent);
    mp.isIOS = /(iphone|ipod|ipad|ios)/i.test(navigator.userAgent);
    mp.isMobile = mp.isAndroid || mp.isIOS || /(mobile)/i.test(navigator.userAgent);

    mp.renderSlider = $.proxy(function(force){
        var station = this.getCurrentStation();
        var group = this.groups[station.altGroup || station.group];
        var holder = $('#mp-wrap');

        var siblings = Object.keys(group.stations);

        if (force === true || $('.js-sibling-station[data-station-code="'+station.code+'"]', holder).length === 0)
        {
            var tpl = $('#mp-slider-item-tpl').html();
            var groupStations = [];
            siblings.forEach(function(code){
                groupStations.push(this[code]);
            }, this.stations);
//			console.log(groupStations);
            holder.closest('.mp-wrap').toggle(groupStations.length > 1);
            if (holder.hasClass('slick-initialized'))
            {
                holder.slick('unslick');
            }
            holder.empty();
            var el = null;
            for (var i = 0; i < (this.isMobile ? 5 : 3); i++) {
                groupStations.forEach(function(station){
                    el = $(tpl);
                    el.find('.js-sibling-station').attr('data-station-code', station['code']);
                    el.find('.mp-slide__image').attr({src: station['logo'], alt: station['title']});
                    holder.append(el);
                });
            }
            el = null;

            s.mp.initSiblingStationsSlider();

//			if (siblings.length > 5)
//			{
            // make current station visible in the slider
//			var slideIndex = Math.floor(siblings.indexOf(station.code) / 3);
//				var slideIndex = siblings.indexOf(station.code);
//				holder.slick('slickGoTo', slideIndex);
//			}
        }

    }, mp);

    mp.setCurrentSliderStation = $.proxy(function(){
        var station = this.getCurrentStation();
        var group = this.groups[station.altGroup || station.group];
        var holder = $('#mp-wrap');

        if ( ! holder.hasClass('slick-initialized'))
            return;

        var siblings = Object.keys(group.stations);

        if (siblings.length > 3)
        {
            // make current station visible in the slider at the first position
            var slideIndex = siblings.indexOf(station.code);
            var siblingElems = holder.children('.slick-list').children('.slick-track').children('.slick-slide');
            var delta = siblingElems.filter('.slick-current').index() - siblingElems.filter('.slick-active').index();
            holder.slick('slickGoTo', slideIndex + (delta > 0 ? delta : 0));
        }

    }, mp);

//	mp.playlistApp = new Vue({
//		el: '.mp-playlist',
//		data: {
//			songs: [],
//			stationTitle: mp.getCurrentStation().title,
//			playlistLink: mp.getCurrentStation().playlistLink
//		}
//	});

    mp.socketUrl = document.location.protocol + '//io.emg.fm';
    mp.currentNamespace = '/current';
    mp.stationSocket = null;
    mp.selectedSocket = null;
    mp.currentSocket = null;

    // текущая песня на основной плашке плеера
    mp.initStationSocket = function(){
//		console.log('initStationSocket');
        this.closeStationSocket();

        var station = this.getCurrentStation();

        if (this.selectedSocket && this.selectedSocket.nsp === station.namespace)
        {
            // Reuse existing socket for this namespace
            this.stationSocket = this.selectedSocket;
        }
        else
        {
            // Create new socket to namespace
            this.stationSocket = this.connectToNamespace(station.namespace, "stationSocket");
        }

        if (station.emptyStream)
        {
            // do nothing
            $('.mp-album_photo').find('img')
                .prop('src', station.logo || this.stubImage);
        }
        else if (station.showPrograms)
        {
            this.stationSocket.on('programs list', this.renderProgramsHandler);
            this.stationSocket.on('program began', this.programBeganHandler);
            this.stationSocket.on('program ended', this.programEndedHandler);

            if (this.stationSocket.connected)
            {
                this.stationSocket.emit('get programs', 1);
            }
        }
        else
        {
            this.stationSocket.on('playlist', this.renderPlaylistHandler);
            this.stationSocket.on('song began', this.songBeganHandler);
            this.stationSocket.on('song ended', this.songEndedHandler);

            if (this.stationSocket.connected)
            {
                this.stationSocket.emit('get playlist', 1);
            }
        }
    };

    mp.offStationSocketHandlers = function(socket){
        socket.off('programs list', this.renderProgramsHandler);
        socket.off('program began', this.programBeganHandler);
        socket.off('program ended', this.programEndedHandler);
        socket.off('playlist', this.renderPlaylistHandler);
        socket.off('song began', this.songBeganHandler);
        socket.off('song ended', this.songEndedHandler);
    };

    mp.closeStationSocket = function() {
        if (this.stationSocket)
        {
            if (this.stationSocket !== this.selectedSocket)
            {
                if (this.selectedSocket || this.currentSocket)
                {
                    // We have another connection to server
                    this.stationSocket.disconnect();
                }
                else
                {
                    // It's last connecton. Keep it until new connection not exists
                    this.offStationSocketHandlers(this.stationSocket);
                    var oldSocket = this.stationSocket;
                    window.setTimeout(function(){
                        oldSocket.disconnect();
                    }, 0);
                }
            }
            else
            {
                this.offStationSocketHandlers(this.stationSocket);
            }
            this.stationSocket = null;
        }
    };

    // текущая песня и плейлист слева от списка каналов
    mp.initSelectedSocket = function(){
//		console.log('initSelectedSocket');
        mp.closeSelectedSocket();

        var station = this.getSelectedStation();

        if (this.stationSocket && this.stationSocket.nsp === station.namespace)
        {
            // Reuse existing socket for this namespace
            this.selectedSocket = this.stationSocket;
        }
        else
        {
            // Create new socket to namespace
            this.selectedSocket = this.connectToNamespace(station.namespace, "selectedSocket");
        }

        if (station.emptyStream)
        {
            $(".mp-left").addClass("mp-left--empty");
        }
        else if (station.showPrograms)
        {
            this.renderNextPrograms([]);
            this.selectedSocket.on('programs list', this.renderSelectedProgramsHandler);
            this.selectedSocket.on('program began', this.programSelectedBeganHandler);
            this.selectedSocket.on('program ended', this.programSelectedEndedHandler);

            if (this.selectedSocket.connected)
            {
                this.selectedSocket.emit('get programs', mp.playlistLimit);
            }
            else
            {
                this.selectedSocket.on('connect', this.onConnectSelectedSocketHandler);
            }
            $(".mp-left").removeClass("mp-left--empty");
        }
        else
        {
            this.renderNextSongs([]);
            this.selectedSocket.on('playlist', this.renderSelectedPlaylistHandler);
            this.selectedSocket.on('playlist with next', this.renderSelectedPlaylistHandler);
            this.selectedSocket.on('song began', this.songSelectedBeganHandler);
            this.selectedSocket.on('song ended', this.songSelectedEndedHandler);
            if (this.useNextSongs)
            {
                this.selectedSocket.on('next songs', this.songSelectedNextHandler);
            }

            if (this.selectedSocket.connected)
            {
                if (this.useNextSongs)
                {
                    this.selectedSocket.emit('get playlist with next', mp.playlistLimit, mp.nextSongsLimit);
//					this.selectedSocket.emit('get next songs', mp.nextSongsLimit);
                }
                else
                {
                    this.selectedSocket.emit('get playlist', mp.playlistLimit);
                }
            }
            else
            {
                this.selectedSocket.on('connect', this.onConnectSelectedSocketHandler);
            }
            $(".mp-left").removeClass("mp-left--empty");
        }
    };

    mp.offSelectedSocketHandlers = function(socket){
        socket.off('programs list', this.renderSelectedProgramsHandler);
        socket.off('program began', this.programSelectedBeganHandler);
        socket.off('program ended', this.programSelectedEndedHandler);
        socket.off('playlist', this.renderSelectedPlaylistHandler);
        socket.off('playlist with next', this.renderSelectedPlaylistHandler);
        socket.off('song began', this.songSelectedBeganHandler);
        socket.off('song ended', this.songSelectedEndedHandler);
        socket.off('next songs', this.songSelectedNextHandler);
        socket.off('connect', this.onConnectSelectedSocketHandler);
    };

    mp.onConnectSelectedSocketHandler = function(){
        var station = mp.getSelectedStation();
        if (station.emptyStream)
        {
            // do nothing
        }
        else if (station.showPrograms)
        {
            this.emit('get programs', mp.playlistLimit);
        }
        else
        {
            if (mp.useNextSongs)
            {
                this.emit('get playlist with next', mp.playlistLimit, mp.nextSongsLimit);
//				this.emit('get next songs', mp.nextSongsLimit);
            }
            else
            {
                this.emit('get playlist', mp.playlistLimit);
            }

        }
    };

    // текущая песня в списке каналов справа от плейлиста
    mp.initCurrentSocket = function(){
        if (this.currentSocket)
        {
            return;
        }
        this.currentSocket = this.connectToNamespace(this.currentNamespace, "currentSocket");

        this.currentSocket.on('songs', this.renderCurrentSongsHandler);
        this.currentSocket.on('programs', this.renderCurrentProgramsHandler);
        this.currentSocket.on('song began', this.songBeganCurrentHandler);
        this.currentSocket.on('song ended', this.songEndedCurrentHandler);
        this.currentSocket.on('program began', this.programBeganCurrentHandler);
        this.currentSocket.on('program ended', this.programEndedCurrentHandler);
    };

    mp.closeSelectedSocket = function() {
        if (mp.selectedSocket)
        {
            if (mp.selectedSocket !== mp.stationSocket)
            {
                mp.selectedSocket.disconnect();
            }
            else
            {
                mp.offSelectedSocketHandlers(mp.selectedSocket);
            }
            mp.selectedSocket = null;
        }
    };

    mp.connectToNamespace = function(namespace, label){

        console.log('['+label+'] connectToNamespace ', namespace);
        if (alertIoEvents) {
            alert('['+label+'] connectToNamespace '+namespace);
        }

        var opts = {};
//		if (Modernizr.websockets)
//		{
//			// Use websocket transport first
//			opts.transports = ['websocket', 'polling'];
//		}

        var socket = io(this.socketUrl + namespace, opts);

        socket.on('connect', function(){
            console.log('['+label+'] IO connected to %s namespace', namespace);
            if (alertIoEvents) {
                alert('['+label+'] IO connected to namespace '+namespace);
            }
        });

        socket.on('connect_error', function(err){
            console.error('['+label+'] IO connect error', err);
            if (alertIoEvents) {
                alert('['+label+'] IO connect error: '+err);
            }
        });

        socket.on('reconnect', function(num){
            console.log('['+label+'] IO reconnect to %s #%d', namespace, num);
            if (alertIoEvents) {
                alert('['+label+'] IO reconnect to '+namespace+' '+num);
            }
        });

        socket.on('reconnect_error', function(err){
            console.error('['+label+'] IO reconnect error', err);
            if (alertIoEvents) {
                alert('['+label+'] IO reconnect error: '+err);
            }
        });

        socket.on('reconnect_failed', function(){
            console.log('['+label+'] IO reconnect failed to station %s', namespace);
            if (alertIoEvents) {
                alert('['+label+'] IO reconnect failed to station: '+namespace);
            }
        });

        socket.on('disconnect', function(){
            console.log('['+label+'] IO disconnect from namespace %s', namespace);
            if (alertIoEvents) {
                alert('['+label+'] IO disconnect from namespace '+namespace);
            }
            // Cleanup namespaces cache in the manager instance
            if (this.io.nsps[this.nsp] === this)
            {
                delete this.io.nsps[this.nsp];
                console.log('Cleanup namespaces cache in the manager instance', namespace, this.io.nsps);
            }
        });

        socket.on('playlist', function(songs){
            console.log('['+label+'] playlist', namespace, songs);
        });

        socket.on('programs list', function(data){
            console.log('['+label+'] programs list', namespace, data);
        });

        socket.on('data', function(data){
            console.log('['+label+'] data', namespace, data);
        });

        socket.on('song began', function(song){
            console.log('['+label+'] song began', namespace, song);
        });

        socket.on('song ended', function(song){
            console.log('['+label+'] song ended', namespace, song);
        });

        socket.on('program began', function(data){
            console.log('['+label+'] program began', namespace, data);
        });

        socket.on('program ended', function(data){
            console.log('['+label+'] program ended', namespace, data);
        });

        return socket;
    };

    mp.playlistItemTpl = $('#mp-playlist-item-tpl').html();
    mp.programListItemTpl = $('#mp-program-list-item-tpl').html();
    mp.nextPlayItemTpl = $('#mp-nextplay-item-tpl').html();

    function createSong(song, current)
    {
        var el = $(mp.playlistItemTpl);

        el.find('.mp-time').html(song.startTime.substr(0, 5));
        el.find('.mp-artist').text(song.info && song.info.artistsName || song.artist);
        el.find('.mp-song').text(song.info && song.info.song || song.song);
        if (song.info && song.info.audio)
        {
            el.data('song-audio', song.info.audio);
        }
        else
        {
            el.addClass('mp-disabled');
        }

        if (current && ! song.stopTime)
        {
            el.addClass('current');
        }

        if (song.info && song.info.likeId)
        {
            el.data('like-id', song.info.likeId);
            mp.markupLikeItemFromStore(el);
        }
        else
        {
            el.children('.mp-like').remove();
        }

        return el;
    }

    function createNextSong(song)
    {
        var el = $(mp.nextPlayItemTpl);
        el.find('.mp-time').html(song.startTime.substr(0, 5));
        el.find('.mp-nextplay__artist').text(song.info && song.info.artistsName || song.artist);
        el.find('.mp-nextplay__song').text(song.info && song.info.song || song.song);

        if (song.info && song.info.image)
        {
            el.find('.mp-nextplay__resize img').prop('src', song.info.image);
        }

        return el;
    }

    function createProgramLink(program)
    {
        if (program.link)
        {
            return $('<a>').text(program.name).prop({href: program.link, target: '_blank', title: program.name});
        }
        else
        {
            return program.name;
        }
    }

    function createDjLink(dj)
    {
        if (dj.link)
        {
            return $('<a>').text(dj.name).prop({href: dj.link, target: '_blank', title: dj.name});
        }
        else
        {
            return dj.name;
        }
    }

    function createDjsLinks(djs)
    {
        var djLinks = [];
        djs.forEach(function(dj){
            var link = createDjLink(dj);
            djLinks.push(link);
            djLinks.push(', ');
        });
        djLinks.pop();
        return djLinks;
    }

    function createProgram(program, current)
    {
        var el = $(mp.programListItemTpl);
        el.find('.mp-time').html(program.startTime.substr(0, 5));
        var programLink = createProgramLink(program);
        el.find('.mp-artist').empty().append(programLink);
        if (program.djs && program.djs.length)
        {
            var djLinks = createDjsLinks(program.djs);
            el.find('.mp-song').empty().append(djLinks);
        }
        else
        {
            el.find('.mp-song').text(program.djsName);
        }
        if (current)
        {
            el.addClass('current');
        }
        return el;
    }

    function createNextProgram(program)
    {
        var el = $(mp.nextPlayItemTpl);
        el.find('.mp-time').html(program.startTime.substr(0, 5));

        {
            var programInfo = [];
            programInfo.push(program.startTime.substr(0, 5));
            programInfo.push('-');
            programInfo.push(program.stopTime.substr(0, 5));
            programInfo.push(' ');
            var programLink = createProgramLink(program);
            programInfo.push(programLink);
            el.find('.mp-nextplay__artist').empty().append(programInfo);
        }

        if (program.djs && program.djs.length)
        {
            var djLinks = createDjsLinks(program.djs);
            el.find('.mp-nextplay__song').empty().append(djLinks);
        }
        else
        {
            el.find('.mp-nextplay__song').text(program.djsName);
        }

        if (program.image)
        {
            el.find('.mp-nextplay__resize img').prop('src',  program.image);
        }

        return el;
    }

    // Handlers for station namespace
    mp.renderPlaylistHandler = $.proxy(function(songs){
        mp.renderCurrentOnlineSong(songs[0]);
    }, mp);

    mp.renderSelectedPlaylistHandler = $.proxy(function(data){
        var songs, nextSongs;
        if (data.ts && data.playlist !== undefined)
        {
            // get playlist with next
            songs = data.playlist || [];
            nextSongs = data.nextSongs || [];
        }
        else
        {
            // get playlist
            songs = data || [];
            nextSongs = [];
        }

        // TODO: Fix this workaround
        if (songs.length === 1)
            return;
        var holder = $('.mp-playlist');
        holder.children('.mp-playlist__item').remove();

        songs.forEach(function(song, index){
            var isCurrent = (index === 0);
            var el = createSong(song, isCurrent);
            holder.append(el);
        });

        mp.renderSelectedOnlineSong(songs[0]);
        if ( ! mp.useNextSongs)
        {
            mp.renderNextSongs([]);
        }
        else
        {
            mp.renderNextSongs(nextSongs);
        }
    }, mp);

    mp.renderProgramsHandler = $.proxy(function(data){
        var programs = data.programs;
        var nowTs = data.ts;
        mp.renderCurrentOnlineProgram(programs[0], nowTs);
    }, mp);

    mp.renderSelectedProgramsHandler = $.proxy(function(data){
        var programs = data.programs;
        var nextPrograms = data.nextPrograms;
        var nowTs = data.ts;

        // TODO: Fix this workaround
        if (data.programs.length === 1)
            return;

        var holder = $('.mp-playlist');
        holder.children('.mp-playlist__item').remove();

        programs.forEach(function(program, index){
            var isCurrent = (index === 0 && program.stopTs > nowTs);
            var el = createProgram(program, isCurrent);
            holder.append(el);
        });

        mp.renderSelectedOnlineProgram(programs[0], nowTs);
        mp.renderNextPrograms(nextPrograms);
    }, mp);

    mp.renderNextSongs = $.proxy(function(songs){
        var holder = $('.mp-nextplay');
        holder.children('.mp-nextplay__item').remove();

        if ( ! songs || ! songs.length)
        {
            holder.hide();
            return;
        }

        songs.forEach(function(songs, index){
            var el = createNextSong(songs);
            holder.append(el);
        });

        holder.show();
    }, mp);

    mp.renderNextPrograms = $.proxy(function(programs){
        var holder = $('.mp-nextplay');
        holder.children('.mp-nextplay__item').remove();

        if ( ! programs || ! programs.length)
        {
            holder.hide();
            return;
        }

        programs.forEach(function(program, index){
            var el = createNextProgram(program);
            holder.append(el);
        });

        holder.show();

    }, mp);

    mp.songBeganHandler = $.proxy(function(song){
        mp.renderCurrentOnlineSong(song);
    }, mp);

    mp.songSelectedBeganHandler = $.proxy(function(song){
        var el = createSong(song, true);
        var holder = $('.mp-playlist');
        holder.children('.current').removeClass('current');
        var title = holder.children('.mp-playlist__title');
        title.after(el);
        holder.children('.mp-playlist__item').slice(this.playlistLimit).remove();
        mp.renderSelectedOnlineSong(song);
        if ( ! mp.useNextSongs)
        {
            mp.renderNextSongs([]);
        }
    }, mp);

    mp.songEndedHandler = $.proxy(function(song){
        mp.renderCurrentOnlineSong();
    }, mp);

    mp.songSelectedEndedHandler = $.proxy(function(song){
        var holder = $('.mp-playlist');
        holder.children('.current').removeClass('current');
        mp.renderSelectedOnlineSong();
        if ( ! mp.useNextSongs)
        {
            mp.renderNextSongs([]);
        }
    }, mp);

    mp.songSelectedNextHandler = $.proxy(function(data){
        mp.renderNextSongs(data.nextSongs || []);
    }, mp);

    mp.programBeganHandler = $.proxy(function(data){
        var program = data.program;
        var nowTs = data.ts;
        mp.renderCurrentOnlineProgram(programs[0], nowTs);
    }, mp);

    mp.programSelectedBeganHandler = $.proxy(function(data){
        var program = data.program;
        var nextPrograms = data.nextPrograms;
        var nowTs = data.ts;

        var el = createProgram(program, true);
        var holder = $('.mp-playlist');
        holder.children('.current').removeClass('current');
        var title = holder.children('.mp-playlist__title');
        title.after(el);
        holder.children('.mp-playlist__item').slice(this.playlistLimit).remove();
        mp.renderSelectedOnlineProgram(programs[0], nowTs);
        mp.renderNextPrograms(nextPrograms);
    }, mp);

    mp.programEndedHandler = $.proxy(function(data){
        mp.renderCurrentOnlineProgram(null, 0);
    }, mp);

    mp.programSelectedEndedHandler = $.proxy(function(data){
        var program = data.program;
        var nextPrograms = data.nextPrograms;
        var nowTs = data.ts;

        var holder = $('.mp-playlist');
        holder.children('.current').removeClass('current');
        mp.renderSelectedOnlineProgram(null, 0);
        mp.renderNextPrograms(nextPrograms);
    }, mp);

    mp.renderCurrentOnlineSong = $.proxy(function(song){
        var group = this.getCurrentGroup();
        var station = this.getCurrentStation();
        var isOnline = song && !song.stopTime;

        var artistName, songTitle, imgPanel, imgOverlay;

        if (isOnline)
        {
            artistName = song.info && song.info.artistsName || song.artist;
            songTitle = song.info && song.info.song || song.song;
//			imgPanel = song.info && song.info.image || station.logo;
            imgPanel = song.info && song.info.image || this.stubImage;
            imgOverlay = song.info && song.info.image || this.stubImage;
        }
        else // song ended
        {
            artistName = station.slogan;
            songTitle = '';
//			imgPanel = station.logo;
            imgPanel = this.stubImage;
            imgOverlay = this.stubImage;
        }

        $('.mp-reading__artist').text(artistName).prop('title', artistName);
        $('.mp-reading__song').text(songTitle).prop('title', songTitle);


        $('.mp-album_photo').find('img')
            .prop('src', imgPanel);
        if ( $('body').width() > 736 || imgPanel != this.stubImage) {
            $('.mp-album_photo').find('img')
                .show();
        } else {
            $('.mp-album_photo').find('img')
                .hide();
        }
    }, mp);

    mp.renderSelectedOnlineSong = $.proxy(function(song){
        var group = this.getSelectedGroup();
        var station = this.getSelectedStation();
        var isOnline = song && !song.stopTime;

        var artistName, songTitle, imgPanel, imgOverlay;

        if (isOnline)
        {
            artistName = song.info && song.info.artistsName || song.artist;
            songTitle = song.info && song.info.song || song.song;
//			imgPanel = song.info && song.info.image || station.logo;
            imgPanel = song.info && song.info.image || this.stubImage;
            imgOverlay = song.info && song.info.image || this.stubImage;
        }
        else // song ended
        {
            artistName = station.slogan;
            songTitle = '';
//			imgPanel = station.logo;
            imgPanel = this.stubImage;
            imgOverlay = this.stubImage;
        }

        $('.mp-broadcast__artist').text(artistName);
        $('.mp-broadcast__song').text(songTitle);
        $('.mp-broadcast__cover').find('img').prop('src', imgOverlay);

        var holder = $('.mp-playlist');
        holder.children('.current').removeClass('current');
    }, mp);

    mp.renderCurrentOnlineProgram = $.proxy(function(program, nowTs){
        var group = this.getCurrentGroup();
        var station = this.getCurrentStation();
        var isOnline = program && (program.stopTs > nowTs);

        var djsName, programName, imgPanel, imgOverlay;

        if (isOnline)
        {
            programName = createProgramLink(program);
            if (program.djs && program.djs.length)
            {
                djsName = createDjsLinks(program.djs);
            }
            else
            {
                djsName = program.djsName || '';
            }
//			imgPanel = program.image || station.logo;
            imgPanel = program.image || this.stubImage;
            imgOverlay = program.image || this.stubImage;
        }
        else // program ended
        {
            programName = '';
            djsName = station.slogan;
//			imgPanel = station.logo;
            imgPanel = this.stubImage;
            imgOverlay = this.stubImage;
        }

        $('.mp-reading__artist').empty().append($('<div>').append(programName).html()).prop('title', program.name);
        $('.mp-reading__song').empty().append($('<div>').append(djsName).html()).prop('title', program.djsName);

        $('.mp-album_photo').find('img')
            .prop('src', imgPanel);
        if ( $('body').width() > 736 || imgPanel != this.stubImage) {
            $('.mp-album_photo').find('img')
                .show();
        } else {
            $('.mp-album_photo').find('img').hide();
        }
    }, mp);

    mp.renderSelectedOnlineProgram = $.proxy(function(program, nowTs){
        var group = this.getSelectedGroup();
        var station = this.getSelectedStation();
        var isOnline = program && (program.stopTs > nowTs);

        var djsName, programName, imgPanel, imgOverlay;

        if (isOnline)
        {
            programName = createProgramLink(program);
            if (program.djs && program.djs.length)
            {
                djsName = createDjsLinks(program.djs);
            }
            else
            {
                djsName = program.djsName || '';
            }
//			imgPanel = program.image || station.logo;
            imgPanel = program.image || this.stubImage;
            imgOverlay = program.image || this.stubImage;
        }
        else // program ended
        {
            programName = '';
            djsName = station.slogan;
//			imgPanel = station.logo;
            imgPanel = this.stubImage;
            imgOverlay = this.stubImage;
        }

        $('.mp-broadcast__artist').empty().append(programName);
        $('.mp-broadcast__song').empty().append(djsName);
        $('.mp-broadcast__cover').find('img').prop('src', imgOverlay);

        var holder = $('.mp-playlist');
        holder.children('.current').removeClass('current');

    }, mp);

    // Handlers for "/current" namespace
    mp.renderCurrentSongsHandler = $.proxy(function(data){
        Object.keys(data).forEach(function(station, index){
            var song = data[station];
            mp.setCurrentSong(station, song);
        });
        data = null;
    }, mp);

    mp.renderCurrentProgramsHandler = $.proxy(function(data){
        Object.keys(data).forEach(function(station, index){
            var program = data[station];
            mp.setCurrentProgram(station, program);
        });
        data = null;
    }, mp);

    // Songs
    mp.songBeganCurrentHandler = $.proxy(function(data){
        mp.setCurrentSong(data.station, data.song);
    }, mp);

    mp.songEndedCurrentHandler = $.proxy(function(data){
        mp.setCurrentSong(data.station);
    }, mp);

    // Programs
    mp.programBeganCurrentHandler = $.proxy(function(data){
        mp.setCurrentProgram(data.station, data.program);
    }, mp);

    mp.programEndedCurrentHandler = $.proxy(function(data){
        mp.setCurrentProgram(data.station);
    }, mp);

    mp.setCurrentSong = $.proxy(function(stationCode, song){
        if ( ! this.stations[stationCode])
            return;
        var holder = $('.js-all-stations-list');
        var el = holder.children('[data-station-code="'+stationCode+'"]');
        var artistName, songTitle;
        if (song)
        {
            artistName = escapeHTML(song.info && song.info.artistsName || song.artist);
            songTitle = escapeHTML(song.info && song.info.song || song.song);
        }
        else
        {
            artistName = this.stations[stationCode].slogan || '&nbsp;';
            songTitle = '&nbsp;';
        }
        el.find('.mp-radio__artist').html(artistName);
        el.find('.mp-radio__song').html(songTitle);
    }, mp);

    mp.setCurrentProgram = $.proxy(function(stationCode, program){
        if ( ! this.stations[stationCode] || this.stations[stationCode].emptyStream)
            return;
        var holder = $('.js-all-stations-list');
        var el = holder.children('[data-station-code="'+stationCode+'"]');
        var djsName, programName;
        if (program)
        {
            programName = createProgramLink(program);
            if (program.djs && program.djs.length)
            {
                djsName = createDjsLinks(program.djs);
            }
            else
            {
                djsName = program.djsName;
            }
        }
        else
        {
            djsName = '&nbsp;';
            programName = this.stations[stationCode].slogan || '&nbsp;';
        }
        el.find('.mp-radio__artist').empty().append(programName);
        el.find('.mp-radio__song').empty().append(djsName);
    }, mp);

    mp.markupLikesFromStore = function(context){
        if (store && store.enabled)
        {
            $('.mp-playlist__item', context).each(function(i, el){
                mp.markupLikeItemFromStore(el);
            });
        }
    };

    mp.markupLikeItemFromStore = function(item){
        if (store && store.enabled)
        {
            var $el = $(item);
            var key = 'mp:'+mp.currentStationCode+':song_'+$el.data('like-id');
            var liked = mp.storeWithExpiration.get(key);
            if (liked === 1)
            {
                $el.find('> .mp-like > .min').removeClass('active');
                $el.find('> .mp-like > .max').addClass('active');
            }
            else if (liked === -1)
            {
                $el.find('> .mp-like > .max').removeClass('active');
                $el.find('> .mp-like > .min').addClass('active');
            }
        }
    };

    mp.clickLikeThumbHandler = function(e){

        e.preventDefault();

        var $this = $(this),
            $holder = $this.closest('.mp-playlist__item'),
            likeId = $holder.data('like-id'),
            likeKey = 'mp:'+mp.currentStationCode+':song_'+likeId;

//		console.log('clickLikeThumbHandler', $holder);

        if ( ! likeId)
            return;

        var likeUrl = mp.getCurrentStation().likeUrl;
        if ( ! likeUrl)
            return;

        if ($holder.data('like-in-process') === true)
            return;

        $holder.data('like-in-process', true);

        var likeTtl = mp.defaultLikeTtl*1000;
        var likeMode = '';
        var likeValue = 0;
        // Find all instances of this like object
//		var $likeBoxes = $holder.add($('.j-likeThumbsBox[data-like-id="'+like_id+'"]'));
        var $likeBoxes = $holder;

        if (store && store.enabled && likeKey)
        {
            // Sync state with localStorage
            var liked = mp.storeWithExpiration.get(likeKey);
            if (liked === 1)
            {
                $likeBoxes.find('> .mp-like > .min').removeClass('active');
                $likeBoxes.find('> .mp-like > .max').addClass('active');
            }
            else if (liked === -1)
            {
                $likeBoxes.find('> .mp-like > .max').removeClass('active');
                $likeBoxes.find('> .mp-like > .min').addClass('active');
            }
        }

        if ($this.hasClass('active') && $this.hasClass('min'))
        {
            // Undo previous dislike
            likeMode = 'undo';
            $likeBoxes.find('> .mp-like > .min').removeClass('active');
        }
        else if ($this.hasClass('active') && $this.hasClass('max'))
        {
            // Undo previous like
            likeMode = 'undo';
            $likeBoxes.find('> .mp-like > .max').removeClass('active');
        }
        else
        {
            // Set new selection
            if ($this.hasClass('max'))
            {
                // It's like action
                likeMode = 'like';
                likeValue = 1;
            }
            else if ($this.hasClass('min'))
            {
                // It's dislike action
                likeMode = 'dislike';
                likeValue = -1;
            }
        }

        if (store && store.enabled && likeKey)
        {
            if (likeMode === 'undo')
            {
                mp.storeWithExpiration.remove(likeKey);
            }
            else if (likeMode === 'like')
            {
                mp.storeWithExpiration.set(likeKey, 1, likeTtl);
            }
            else if (likeMode === 'dislike')
            {
                mp.storeWithExpiration.set(likeKey, -1, likeTtl);
            }
        }

        if (likeMode === 'undo')
        {
            // Cleanup any like selection
            $likeBoxes.find('> .mp-like > .active').removeClass('active');
        }
        else if (likeMode === 'like')
        {
            // Setup like selection
            $likeBoxes.find('> .mp-like > .min').removeClass('active');
            $likeBoxes.find('> .mp-like > .max').addClass('active');
        }
        else if (likeMode === 'dislike')
        {
            // Setup dislike selection
            $likeBoxes.find('> .mp-like > .max').removeClass('active');
            $likeBoxes.find('> .mp-like > .min').addClass('active');
        }

        mp.executeLikeAction(likeUrl, likeId, likeValue)

            .done(function(data){
                if (data.success)
                {
                    // Do nothing
                }
                else
                {
                    s.showErrorAlert(data.error || "Ошибка голосования");
                }
            })
            .always(function(){
                $holder.data('like-in-process', false);
            });

//		if (s.counters && s.counters.hit)
//		{
//			s.counters.hit(likeUrl+'&id='+likeId+'&like='+likeValue, 'LIKE - '+document.title, s.referer);
//		}

    };

    mp.executeLikeAction = function(likeUrl, likeId, likeValue){
        return $.ajax({
            "url" : likeUrl,
            "data" : {id: likeId, like: likeValue},
            "type": "POST",
            "dataType": "json"
        })
            .fail(function(jqXHR, textStatus, errorThrown) {
                var message = "Не удалось выполнить запрос";
                if (textStatus === "error")
                {
                    message += "\n"+errorThrown;
                }
                s.showErrorAlert(message);
            });
    };

    mp.storeWithExpiration = {
        set: function(key, val, exp){
            store.set(key, { _val:val, _exp:exp, _time:new Date().getTime() })
        },
        get: function(key) {
            var info = store.get(key);
            if ( ! info)
                return null;
            if ( ! info._time)
                return null;
            if (new Date().getTime() - info._time > info._exp)
            {
                store.remove(key);
                return null;
            }
            return info._val;
        },
        remove: function(key) {
            store.remove(key);
        },
        removeExpired: function() {
            var now = new Date().getTime();
            $.each(store.getAll(), function(key, val) {
                if (val._time && now - val._time > val._exp)
                {
                    store.remove(key);
                }
            });
        }
    };

})(s, s.mp, jQuery, io, store);

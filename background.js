
var g_iTabId = 0;
var g_sLast = '';
var g_sLastNotificationId = '';

function sendNotification(title, message)
{
    console.log('Send notification: ' + title + ' ' + message);
    var notificationId = "Europa plus song info " + Date.now().toString();
    chrome.notifications.create(notificationId, {
        type: "basic",
        iconUrl: "images/logo_ep.svg",
        title: title,
        message: message,
        eventTime: Date.now() + 5000
    });
    g_sLastNotificationId = notificationId;
}

function getSongInfo(FunctionCallBack)
{
    if (g_iTabId !== 0) {
            chrome.tabs.executeScript(g_iTabId, {code: "[artist.text(), song.text(), photo.attr('src')]"}, function (results) {
                if(results[0] !== null) {
                    FunctionCallBack(results);
                }
            });
    }
}

function displayInfo(force = false)
{
    if(g_iTabId !== 0) {
            getSongInfo(function (results) {
                console.log(results);
                if(g_sLast !== results[0][1] || force) {
                    g_sLast = results[0][1];
                    sendNotification(results[0][0], results[0][1])
                }else {
                    if(g_sLastNotificationId) {
                        console.log('Clear notification: ' + g_sLastNotificationId);
                        chrome.notifications.clear(g_sLastNotificationId);
                        g_sLastNotificationId = '';
                    }
                }

            });
    }
}

chrome.alarms.create("Start", {periodInMinutes:1});
chrome.alarms.onAlarm.addListener(function(alarm){
    console.log('alarm.');
    updatePopup();
    displayInfo();
});

function newTab(tabId)
{
    console.log('New Tab ID: ' + tabId);
    chrome.tabs.executeScript(tabId, {file: "jquery-3.3.1.min.js"}, function () {
        console.log('JQuery injected');
        chrome.tabs.executeScript(tabId, {file: "content.js"}, function () {
            console.log('Content injected');
            g_iTabId = tabId;
            //setVolume(0.2206);
        });
    });
    displayInfo();
}


chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if(changeInfo.status === 'complete'
        && tab.url.indexOf('http://www.europaplus.ru/') === 0
        && (g_iTabId === 0 || g_iTabId === tabId)) {
            newTab(tabId);
    }
});

chrome.tabs.onRemoved.addListener(function (tabId, removeInfo) {
    if(g_iTabId === tabId) {
        g_iTabId = 0;
        console.log('Remove Tab ID: ' + tabId);
    }
});

chrome.tabs.query({url: 'http://www.europaplus.ru/*', status: 'complete'}, function (result) {
    if(result.length !== 0) {
        newTab(result[0].id);
    }
});

function playerPlay()
{
    if(g_iTabId !== 0) {
        chrome.tabs.executeScript(g_iTabId,
            //{code: "$('div.mp-button.js-btn-online').click();"},
            {code: "playerToggle();"},
            //function () {playerState();}
        );
    }
}

function playerState()
{
    if(g_iTabId !== 0) {
        chrome.tabs.executeScript(g_iTabId,
            {code: "[mp_button.attr('class'), getVolume()]"},
            //{code: "[player_audio.paused, player_audio.volume]"},
            function (results) {
                console.log(results);
                if(results[0] !== null) {
                    chrome.runtime.sendMessage({
                        from: 'background',
                        action: 'play',
                        state: results[0][0].indexOf('js-stop') !== -1 ? 'Stop' : 'Play',
                        //state: results[0][0] ? 'Play' : 'Stop',
                        volume: results[0][1]
                    });
                }
            });
    }
}

function updatePopup()
{
    getSongInfo(function (result) {
        chrome.runtime.sendMessage({
            from: 'background',
            action: 'update',
            artist: result[0][0],
            song: result[0][1],
            photo: result[0][2]
        });
    });
}

function setVolume(value)
{
    console.log("setVolume " + value);
    if(g_iTabId !== 0) {
        chrome.tabs.executeScript(g_iTabId,
            {code: "setVolume(" + value + ");"},
        );
    }
}

chrome.runtime.onMessage.addListener( function(message, sender, sendResponse) {
    if(message.from && message.from === 'popup'){
        switch(message.action){
            case 'click':
                displayInfo(true);
                break;
            case 'update':
                updatePopup();
                playerState();
                break;
            case 'play':
                playerPlay();
                break;
            case 'volume':
                setVolume(message.value);
                break;
        }
    }
});
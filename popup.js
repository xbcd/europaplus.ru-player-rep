document.addEventListener('DOMContentLoaded', function() {
    chrome.runtime.sendMessage(
        {from: "popup", action: "update"}
    );
    /*
    document.getElementById('main_button').addEventListener('click', function() {
        chrome.runtime.sendMessage(
            {from: "popup", action: "click"},
        );
    });
    */
    var play_button = document.getElementById('play_button');
    play_button.addEventListener('click', function() {
        chrome.runtime.sendMessage(
            {from: "popup", action: "play"},
        );

    });

    $('#play_button').click(function (e) {
        $(this).toggleClass('b-stop');
    });

    var slider = document.getElementById('slider');

    chrome.runtime.onMessage.addListener( function(message, sender, sendResponse) {
        if(message.from && message.from === "background") {
            switch(message.action){
                case 'update':
                    document.getElementById('main_artist').innerText = message.artist;
                    document.getElementById('main_song').innerText = message.song;
                    document.getElementById('main_photo').setAttribute(
                        'src',
                        message.photo.indexOf('http') === 0 ? message.photo : 'images/logo_ep.svg');
                    break;
                case 'play':
                    $('#play_button').attr('class', message.state === 'Stop' ? 'b-stop' : '');
                    setVolume(message.volume);
                    noUiSlider.create(slider, {
                        start: message.volume,
                        connect: [true, false],
                        orientation: "horizontal",
                        range: {
                            'min': 0,
                            'max': 1
                        },
                    });

                    // Bind the color changing function to the update event.
                    slider.noUiSlider.on('update', function () {
                        setVolume(slider.noUiSlider.get());
                    });
                    break;
            }
        }
    });
});

function setVolume(value)
{
    chrome.runtime.sendMessage({
        from: "popup",
        action: "volume",
        value: value
    });
}
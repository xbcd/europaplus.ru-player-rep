

var artist = $('html > body.push--inited > div#mp-app.mp > div.mp-panel > div.mpc > div#mp-cover.mp-cover > div.mp-reading > div.mp-reading__artist');
var song = $('html > body.push--inited > div#mp-app.mp > div.mp-panel > div.mpc > div#mp-cover.mp-cover > div.mp-reading > div.mp-reading__song');
var photo = $('html > body.push--inited > div#mp-app.mp > div.mp-panel > div.mpc > div#mp-cover.mp-cover > div.mp-album > span.mp-album_photo > img.js-station-logo');


var mp_button = $('div.mp-button.js-btn-online');

function playerToggle()
{
    mp_button.click();
}

function getVolume()
{
    return document.getElementsByTagName('audio')[0].volume;
}

function setVolume(value)
{
    document.getElementsByTagName('audio')[0].volume = value;
}